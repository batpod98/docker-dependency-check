# Changelog

## v1.16.8 (2023-07-09)

### Fixed

- Added N/A suppression for `wordwrap` CVE-2023-26115 since no known uses are
  susceptible.

## v1.16.7 (2023-07-02)

### Fixed

- Added N/A suppression for `semver` CVE-2022-25883 since no known uses are
  susceptible.

## v1.16.6 (2023-06-12)

### Fixed

- Updated to OWASP Dependency Check [v8.3.1](https://github.com/jeremylong/DependencyCheck/releases/tag/v8.3.1)

## v1.16.5 (2023-03-25)

### Fixed

- Added N/A suppression for CVE-2023-26115 in `word-wrap@1.2.3`, a dependency
  of ESLint.

## v1.16.4 (2023-03-24)

### Fixed

- Updated to OWASP Dependency Check [v8.2.1](https://github.com/jeremylong/DependencyCheck/releases/tag/v8.2.1)
- Removed unused suppressions from `npm_na_suppressions.xml` and
  `npm_fp_suppressions.xml`, which now have no suppressions. (#46)

## v1.16.3 (2023-03-12)

### Fixed

- Removed suppressions for nested NPM dependencies. Fixed in
  [OWASP Dependency Check v7.4.2](https://github.com/jeremylong/DependencyCheck/issues/5116#issuecomment-1344966711).
  (#43)

## v1.16.2 (2023-02-28)

### Fixed

- Updated to OWASP Dependency Check [v8.1.2](https://github.com/jeremylong/DependencyCheck/releases/tag/v8.1.2)

## v1.16.1 (2023-02-13)

### Fixed

- Updated to OWASP Dependency Check [v8.1.0](https://github.com/jeremylong/DependencyCheck/releases/tag/v8.1.0)

## v1.16.0 (2023-01-16)

### Changed

- Updated to OWASP Dependency Check
  [v8.0.0](https://github.com/jeremylong/DependencyCheck/releases/tag/v8.0.0).
  The breaking change in v8 was an update to the database schema, which is
  localized to the image and should not be impacting. If Dependency Check is
  run in an offline mode see the hosted suppression file change in the
  [release notes](https://github.com/jeremylong/DependencyCheck/releases/tag/v8.0.0)
  for potential impacts.

### Fixed

- Updated example in README to remove unneeded check and add example using the
  image's suppression files.

## v1.15.9 (2023-01-06)

### Fixed

- Updated to OWASP Dependency Check [v7.4.4](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.4.4)

## v1.15.8 (2022-12-29)

### Fixed

- Updated to OWASP Dependency Check [v7.4.3](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.4.3)

## v1.15.7 (2022-12-28)

### Fixed

- Updated to OWASP Dependency Check [v7.4.2](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.4.2)

## v1.15.6 (2022-12-25)

### Fixed

- Added NA suppression for deeply nested npm dependencies, per
  [ODC issue #5116](https://github.com/jeremylong/DependencyCheck/issues/5116#issuecomment-1344966711).
  This suppresses any NPM package with a range in the version,
  instead of one specific version. (#42)

## v1.15.5 (2022-12-09)

### Fixed

- Updated to OWASP Dependency Check [v7.4.1](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.4.1)
- Removed FP suppression for CVE-2022-3517 in `minimatch@3.0.4` to avoid masking Dependency Check bug fixed in 7.4.1.

## v1.15.4 (2022-12-04)

### Fixed

- Added FP suppression for CVE-2022-3517 in `minimatch@3.0.4` with OWASP
  Dependency Check v7.4.0. The package defines `^3.0.4` as the dependency,
  but `3.1.2` is installed. See the
  [FP report](https://github.com/jeremylong/DependencyCheck/issues/5116). (#41)

## v1.15.3 (2022-12-04)

### Fixed

- Updated to OWASP Dependency Check [v7.4.0](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.4.0)

## v1.15.2 (2022-11-30)

### Fixed

- Fixed error preventing `schedule` pipelines from running daily and seeding
  the vulnerability database. (#40)

## v1.15.1 (2022-11-19)

### Fixed

- Updated to OWASP Dependency Check [v7.3.2](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.3.2)

## v1.15.0 (2022-11-17)

### Changed

- Added standard set of `LABEL`s to image. (#39)

### Miscellaneous

- Updated the CI pipeline to have long-running jobs use GitLab shared `medium` sized
  runners and optimized needs, both to reduce pipeline execution times. (#38)
- Updated documentation on container image tags.

## v1.14.5 (2022-10-19)

### Fixed

- Updated to OWASP Dependency Check [v7.3.0](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.3.0)

## v1.14.4 (2022-09-23)

### Fixed

- Updated to OWASP Dependency Check [v7.2.1](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.2.1), resolving issue https://github.com/jeremylong/DependencyCheck/issues/4846. (#36)

## v1.14.3 (2022-09-14)

### Fixed

- Updated to OWASP Dependency Check [v7.2.0](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.2.0)

## v1.14.2 (2022-08-20)

### Fixed

- Updated to OWASP Dependency Check [v7.1.2](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.1.2)

## v1.14.1 (2022-06-12)

### Fixed

- Updated to OWASP Dependency Check [v7.1.1](https://github.com/jeremylong/DependencyCheck/releases/tag/v7.1.1)

## v1.14.0 (2022-05-29)

### Changed

- Suppress false positive for CVE-2022-21944 in `fb-watchman@2.0.1`. Only affects openSUSE Backports SLE-15-SP3 watchman versions prior to 4.9.0, and not running any openSUSE. (#34)
- Suppress false positive (N/A) for CVE-2021-23362 in `hosted-git-info@2.8.9`. ReDoS vulnerability in dependency of eslint-config-unicorn (dev-only). (#35)

## v1.13.0 (2022-05-03)

### Changed

- Updated to OWASP Dependency Check v7.1.0
- Removed all previous global suppressions since all are now deperecated with fixes available. (#12)

## v1.12.4 (2022-03-08)

### Changed

- Update suppression for `glob-parent@2.0.0` (from `htmlhint`, CVE-2020-28469) to include all NPM advisories. If there is a new vulnerability for this dependency, should have a new CWE or CVE.

## v1.12.3 (2022-03-07)

### Changed

- Add [another NPM advisory](https://github.com/advisories/GHSA-ww39-953v-wcq6) suppression for `glob-parent@2.0.0` from `htmlhint` (CVE-2020-28469) (#32)

## v1.12.2 (2022-03-06)

### Changed

- Add [another NPM advisory](https://github.com/advisories/GHSA-ww39-953v-wcq6) suppression for `glob-parent@2.0.0` from `htmlhint` (CVE-2020-28469) (#31)

## v1.12.1 (2022-03-04)

### Changed

- Add [another NPM advisory](https://github.com/advisories/GHSA-ww39-953v-wcq6) suppression for `glob-parent@2.0.0` from `htmlhint` (CVE-2020-28469) (#30)

## v1.12.0 (2022-02-20)

### Changed

- Removed suppression for CVE-2022-21670 for `markdown-it`, fixed in v12.3.2 (#28)

## v1.11.3 (2022-02-09)

### Changed

- Add [another NPM advisory](https://github.com/advisories/GHSA-ww39-953v-wcq6) suppression for `glob-parent@2.0.0` from `htmlhint` (CVE-2020-28469) (#29)

## v1.11.2 (2022-01-19)

### Changed

- Add [yet another NPM advisory](https://github.com/advisories/GHSA-6vfc-qv3f-vr6c) suppression for [`markdown-it`](https://github.com/DavidAnson/markdownlint/issues/478)

## v1.11.1 (2022-01-18)

### Changed

- Added CVE-2022-21670 to suppression for [`markdown-it`](https://github.com/DavidAnson/markdownlint/issues/478)

## v1.11.0 (2022-01-16)

### Changed

- Added suppression for CVE-2022-21670 for [`markdown-it`](https://github.com/DavidAnson/markdownlint/issues/478). (#27)

## v1.10.3 (2021-12-18)

### Changed

- Updated to OWASP Dependency Check v6.5.1

## v1.10.2 (2021-12-09)

### Changed

- Change suppression for `glob-parent@5.2.1` to `glob-parent@6.0.2` (#26)

## v1.10.1 (2021-11-20)

### Changed

- Updated suppression for `glob-parent@5.2.1` to include new NPM/GitHub advisory number (#25)

## v1.10.0 (2021-11-18)

### Changed

- Updated suppression for `glob-parent@2.0.0` with new NPM/GitHub advisory number. (#24)

## v1.9.0 (2021-11-08)

### Changed

- Updated to OWASP Dependency Check v6.5.0

## v1.8.0 (2021-10-11)

### Changed

- Updated to OWASP Dependency Check v6.4.1

## v1.7.1 (2021-10-08)

### Changed

- Updated suppression for `glob-parent@5.2.1` to include new NPM/GitHub advisory number (#23)

## v1.7.0 (2021-10-08)

### Changed

- Updated suppression for `glob-parent@2.0.0` with new NPM/GitHub advisory number. (#22)
- Move to container build/test/deploy pipeline leveraging `kaniko` for build and `skopeo` for deploy. (#21)

## v1.6.0 (2021-10-01)

### Changed

- Added false positive suppression for CVE-2021-41720 for `lodash`, see https://github.com/lodash/lodash/issues/5261. (#20)

## v1.5.0 (2021-09-29)

### Changed

- Updated to OWASP Dependency Check v6.3.2

## v1.4.0 (2021-09-28)

### Changed

- Added suppression for `ansi-regex@2.1.1` and `ansi-regex@4.1.0` since not applicable. (#19)

## v1.3.0 (2021-09-01)

### Changed

- Added suppression for `glob-parent@5.2.1` since [false positive](https://github.com/jeremylong/DependencyCheck/issues/3621) (#17)
- Updated to OWASP Dependency Check v6.3.1

## v1.2.0 (2021-08-28)

### Changed

- Updated suppressions for `glob-parent` (from `htmlhint`) since only development ReDoS vulnerability (#16)

### Miscellaneous

- Setup renovate for dependency updates (#15)

## v1.1.9 (2021-08-22)

### Fixed

- Removed suppression for `postcss@7.0.36` since found to not be susceptible (#9)

### Miscellaneous

- Execute Dependency Check on image to test execution before deploy (#14)

## v1.1.8 (2021-06-26)

### Changed

- Updated `postcss` suppressions for `stylelint` (#13)

## v1.1.7 (2021-06-20)

### Changed

- Added suppression for `glob-parent` (from `htmlhint`) since only development ReDoS vulnerability (#11)
- Updated `postcss` suppressions for latest version of `stylelint` (#7, #10)

### Fixed

- Removed suppression for `path-parse@1.0.6` since fixed in `1.0.7` (#8)

## v1.1.6 (2021-05-23)

### Fixed

- Update suppression lists to remove NA CVEs that have been resolved by the packages and FP CPEs that are resolved with new CPE mapping algorithm (#10)

## v1.1.5 (2021-05-16)

### Changed

- Added suppression for `path-parse@1.0.6` since only dev impact from `eslint` and `jest` (#6)
- Added suppression for `postcss@7.0.35` since only dev impact from `stylelint` (#7)

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#5)

## v1.1.4 (2020-11-01)

### Changed

- Updated suppression files for additional npm false positive CPEs by name (#2)
- Updated suppression files for `trim` module since only dev dependency for `stylelint` (#4)

## v1.1.3 (2020-09-22)

### Changed

- Updated suppression files for false positive CPEs for `cacheable-lookup` (#3)

## v1.1.2 (2020-09-16)

### Fixed

- Fixed suppression files for npm false positive CPEs to not exclude the applicable npm modules (#2)

## v1.1.1 (2020-09-14)

### Changed

- Updated suppression files for npm false positive CPEs (#1)

## v1.1.0 (2020-09-14)

### Added

- Added new suppression files for npm false positive CPEs and not applicable CVEs (#1)

## v1.0.0 (2019-11-15)

### Initial Release

- Initial container with OWASP Dependency Check image pre-seeded with the latest database updates
