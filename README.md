# Docker OWASP Dependency Check

A Docker image to run the [OWASP Dependency Check](https://www.owasp.org/index.php/OWASP_Dependency_Check)
tool. It is built from the [Dependency Check project's image](https://hub.docker.com/r/owasp/dependency-check),
but pre-seeded with the latest database updates (updated at least daily).

## GitLab CI Usage

The following shows an example GitLab CI job using this image to run OWASP Dependency Check.

```yaml
owasp_dependency_check:
  image:
    name: registry.gitlab.com/gitlab-ci-utils/docker-dependency-check:latest
    entrypoint: [""]
  stage: test
  script:
    # Job will scan the project root folder and fail if any vulnerabilities with CVSS > 0 are found
    - >
      /usr/share/dependency-check/bin/dependency-check.sh --scan "./" --format ALL
      --project "$CI_PROJECT_NAME" --failOnCVSS 0
  allow_failure: true
  artifacts:
    when: always
    paths:
        # Save the HTML and JSON report artifacts
      - "./dependency-check-report.html"
      - "./dependency-check-report.json"
```

See the [documentation](https://jeremylong.github.io/DependencyCheck/dependency-check-cli/arguments.html)
for details on the available command line arguments.

## Suppression Files

The following suppression files for false positives are included in this image in the
`/suppressions` folder.

- `npm_fp_suppression.xml`: includes suppressions for false positives for npm modules
  (i.e. flagged as the wrong CPE, always false positives)
- `npm_na_suppressions.xml`: includes suppressions for npm modules with vulnerabilities
  are not applicable (i.e. correct CPEs and CVEs, but CVEs are not applicable for these
  specific versions)

To use these suppressions, update the job script to be:

```yaml
  script:
    - >
      /usr/share/dependency-check/bin/dependency-check.sh --scan "./" --format ALL
      --project "$CI_PROJECT_NAME" --failOnCVSS 0
      --suppression /suppressions/npm_fp_suppression.xml
      --suppression /suppressions/npm_na_suppressions.xml
```

## Docker Dependency Check Images

All available Docker image tags can be found in the `gitlab-ci-utils/docker-dependency-check` repository at https://gitlab.com/gitlab-ci-utils/docker-dependency-check/container_registry. Details on each release can be found on the [Releases](https://gitlab.com/gitlab-ci-utils/docker-dependency-check/releases) page.

The following tag conventions are used:

- `latest`: Based on the latest OWASP Dependency Check image, rebuilt daily to pre-seed the vulnerability database.
- `x.y.z`: The image last built with a specific release of this project, as detailed on the [Releases](https://gitlab.com/gitlab-ci-utils/docker-dependency-check/releases) page. Note these images are **not** rebuilt.

**Note:** Any images in the `gitlab-ci-utils/docker-dependency-check/tmp` repository are temporary images used during the build process and may be deleted at any point.
