FROM owasp/dependency-check:8.3.1@sha256:06fa5954ab7bbef34864807d9b3477d50116ce7bdd8dc1faebecf45750739fb0

COPY suppressions/ /suppressions/

RUN /usr/share/dependency-check/bin/dependency-check.sh --updateonly

LABEL org.opencontainers.image.licenses="Apache-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/docker-dependency-check"
LABEL org.opencontainers.image.title="docker-dependency-check"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/docker-dependency-check"

ENTRYPOINT ["/usr/share/dependency-check/bin/dependency-check.sh"]
CMD ["--scan","/builds","--format","ALL","--project","GENERIC","--failOnCVSS","0"]
